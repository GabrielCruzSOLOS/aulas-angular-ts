import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  authForm:FormGroup;

  constructor(private fb:FormBuilder, private auth:AuthService, private router:Router){ }

  ngOnInit(): void {
    this.authPlatform()
  }


  authPlatform(){
    this.authForm = this.fb.group({
      email: ['', Validators.email],
      password: ['', Validators.min(5) ]
    })
  }

  sendAuth() {

    this.auth.authAPI(this.authForm.value)
    this.authForm.reset();
    console.log(localStorage.getItem('token'))
    this.router.navigate(["form"])
  }
}
