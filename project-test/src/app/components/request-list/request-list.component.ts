import { Component, OnDestroy, OnInit } from '@angular/core';
import { Form } from 'src/app/models/form';
import { RequestServiceService } from 'src/app/services/request-service.service';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.css']
})
export class RequestListComponent implements OnInit, OnDestroy {

  list: Array<Form> = new Array()


  constructor(private restList:RequestServiceService) { }

  ngOnInit(): void {
    this.listRequests()
  }

  listRequests(){
    this.restList.requestListAPI().subscribe(list => {
      this.list = list;
    }, err => {
      console.log("Erro ao listar!", err)
    }
    )
  }

  ngOnDestroy() {
    console.log("DestroiRequestList")
  }

}
