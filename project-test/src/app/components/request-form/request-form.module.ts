import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestFormRoutingModule } from './request-form-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RequestFormComponent } from './request-form.component';
import { AreasService } from 'src/app/services/areas.service';

@NgModule({
  declarations: [RequestFormComponent],
  imports: [
    CommonModule,
    RequestFormRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  providers: [AreasService]
})
export class RequestFormModule { }
