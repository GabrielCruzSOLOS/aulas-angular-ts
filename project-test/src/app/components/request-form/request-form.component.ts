import { Component, createPlatform, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AreasService } from 'src/app/services/areas.service';
import { RequestServiceService } from 'src/app/services/request-service.service';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.css']
})
export class RequestFormComponent implements OnInit, OnDestroy {
  requestForm:FormGroup;
  areas: string[];
  schalarityOp = ['Ensíno Médio Completo', 'Ensíno Superior Incompleto', 'Ensíno Superior Completo']
  qtdRequest:number = 1

  constructor(private fb:FormBuilder, private areasService: AreasService, private rest:RequestServiceService) { }

  ngOnInit(): void {
    this.createPlatform()
    this.areas = this.areasService.getAreas();
  }

  createPlatform(){

    this.requestForm = this.fb.group({
      areas: ['', Validators.required],
      amount: [1, Validators.required],
      scholarity: ['', Validators.required],
      note: ['', Validators.required]
    })
  }

  addQtdRequest(){
    this.requestForm.get('amount').setValue(this.qtdRequest++)
  }

  subQtdRequest(){
    this.requestForm.get('amount').setValue(this.qtdRequest ? this.qtdRequest-- : this.qtdRequest=1)
  }

  sendRequest(){
    this.rest.requestFormAPI(this.requestForm.value).subscribe(result => {
      this.requestForm.reset()
      this.qtdRequest = 1;
      //fazer um modal de enviado com sucesso!
    });
    this.createPlatform()
  }

  cancelRequest(){
    this.requestForm.reset()
    this.createPlatform()
    this.qtdRequest = 1;
  }

  ngOnDestroy(): void {
    console.log("Destroy form")
  }

}
