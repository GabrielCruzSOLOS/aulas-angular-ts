import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { AuthenticationComponent } from './layout/authentication/authentication.component';
import { MainComponent } from './layout/main/main.component';

const routes: Routes = [
  {
    path:'',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {path: "form", loadChildren: () => import ("./components/request-form/request-form.module").then((m) => m.RequestFormModule)},
      {path: "list", loadChildren: () => import ("./components/request-list/request-list.module").then((m) => m.RequestListModule)}
    ]
  },
  {
    path: 'authentication',
    component: AuthenticationComponent,
    loadChildren: () =>
    import('./components/authentication/authentication.module').then(
      (m) => m.AuthenticationModule
    ),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
