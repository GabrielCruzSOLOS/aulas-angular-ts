import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AreasService {

  constructor() {}

  getAreas(){
    //Dados mocados para o primeiro teste do service!
    return ['JPA', 'Angular', 'PostgreSQL', 'Git Actions'];
  }

}
