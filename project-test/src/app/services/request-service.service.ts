import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Form } from '../models/form';


@Injectable({
  providedIn: 'root'
})
export class RequestServiceService {
  apiUrl = 'http://localhost:8080/solicitar';
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };


  constructor(private httpClient: HttpClient) { }

  public requestFormAPI(form: any): Observable<Form> {
    return this.httpClient.post<any>(this.apiUrl, form, this.httpOptions);
  }

  public requestListAPI(): Observable<any> {
    return this.httpClient.get(this.apiUrl);
  }
}
