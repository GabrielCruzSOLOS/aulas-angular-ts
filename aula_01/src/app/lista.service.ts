import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { ColabotadorModel } from './form/colaborador.model';

@Injectable({
  providedIn: 'root'
})
export class ListaService {

  constructor(private http: HttpClient) { }

  listarColaboradores(): Observable<any>{
    return this.http.get("http://localhost:3000/solicitar/")
  }

  cadastrarColaborador(form:ColabotadorModel): Observable<any>{
    return this.http.post("http://localhost:3000/solicitar/", form)
  }
}
