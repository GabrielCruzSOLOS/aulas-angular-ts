import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ColabotadorModel } from '../form/colaborador.model';
import { ListaService } from '../lista.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit, AfterViewInit {

  listar: Array<ColabotadorModel> = new Array()
666

  constructor(private listaService: ListaService) {}

  ngOnInit(): void {
    this.listarColaboradores();
  }

  ngAfterViewInit(){
    console.log("Lista")
  }

  listarColaboradores(){
    this.listaService.listarColaboradores().subscribe(listar => {
      this.listar = listar;
    }, err => {
      console.log("Erro ao listar!", err)
    }
    )
  }


}
