import { Component, Output, EventEmitter, OnInit, AfterViewInit } from '@angular/core';
import { ListaService } from '../lista.service';
import { ListaComponent } from '../lista/lista.component';
import { ColabotadorModel } from './colaborador.model';
import {AreasService} from './areas.service'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})

export class FormComponent implements OnInit, AfterViewInit{

  colaborador: ColabotadorModel = new ColabotadorModel();

  //Comunicação de saída com outro componente!
  @Output() listarComponents:EventEmitter<any> = new EventEmitter

  areas: string[] | undefined;

  areas2: string[];

  areas3: string[] = [];

  areas4: string[] = new Array;

  //ou aqui 02 sem precisar dessa linha de cima e apagando o ngInit?
  //areas: string[] = this.areasService.getAreas();

  constructor(private listaService: ListaService, private listaComponent: ListaComponent, private areasService: AreasService){}

  ngOnInit(): void {
    //aqui 01
    this.areas = this.areasService.getAreas();
  }

  ngAfterViewInit(){
    console.log("form")
  }

  solicitar() {
    this.listaService.cadastrarColaborador(this.colaborador).subscribe(form => {
      alert("cadastrado com sucesso");
      this.listaComponent.listarColaboradores()
    },
    err => {
      console.log("Erro ao cadastrar!", err)
    })
  }

}
