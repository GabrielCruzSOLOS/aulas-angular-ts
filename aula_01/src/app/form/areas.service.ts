import { Injectable } from '@angular/core';

@Injectable()
export class AreasService {

  constructor() {}

  getAreas(){
    return ['JPA', 'Angular', 'PostgreSQL', 'Git Actions'];
  }

}
