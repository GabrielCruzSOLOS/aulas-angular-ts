import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FormComponent } from './form/form.component';
import { ListaComponent } from './lista/lista.component';
import { ListaService } from './lista.service';
import { AreasService } from './form/areas.service';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {path: '', component: FormComponent},
  {path: 'solicitar', component: FormComponent},
  {path: 'solicitacoes', component: ListaComponent}

];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FormComponent,
    ListaComponent,

  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [ListaService, HttpClientModule, ListaComponent, AreasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
