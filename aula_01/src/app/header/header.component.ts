import { AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements AfterViewInit {

  logo:string = "http://solosbrasil.com/wp-content/uploads/2021/06/logo-solos.svg"

  ngAfterViewInit(){
    console.log("header")
  }

}
