import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { RequestDetailsComponent } from './request-details/request-details.component';
import { ListRoutingModule } from 'src/app/components/list/list.routing.module';



@NgModule({
  declarations: [
    ListComponent,
    RequestDetailsComponent
  ],
  imports: [
    CommonModule,
    ListRoutingModule
  ]
})
export class ListModule { }
