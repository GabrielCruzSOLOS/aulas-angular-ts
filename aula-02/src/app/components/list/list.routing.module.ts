import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list.component';
import { RequestDetailsComponent } from './request-details/request-details.component';


const listRoutes: Routes = [
  {path: '', component: ListComponent,
  //Rota filha
  children: [
    {path: ':id', component: RequestDetailsComponent}
  ]},
  //{path: 'list/:id', component: RequestDetailsComponent}
];

@NgModule({
  //Ele deixa de ser forRoot para ser forChild
  imports: [RouterModule.forChild(listRoutes)],
  exports: [RouterModule]
})
export class ListRoutingModule { }
