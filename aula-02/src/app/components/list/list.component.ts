import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  variavelQtd:number = 0

  @Output() valor = new EventEmitter();

  @ViewChild('viewchild') valorViewChild!: ElementRef;


  funcao(){
    console.log("essa: "+this.valorViewChild.nativeElement.value)
  }

  add(){
    this.variavelQtd++;
    this.valor.emit(this.variavelQtd)
  }

  sub(){

    this.variavelQtd ? this.variavelQtd-- : this.variavelQtd=0
    this.valor.emit(this.variavelQtd)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
