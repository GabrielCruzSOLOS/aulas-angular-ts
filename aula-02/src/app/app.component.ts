import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  variavel2: boolean = true;

  emissor:string = 'Input Properties'
  informacoes: any='0';

  hello:boolean =  false

  valorRecebido(event:any){
    this.informacoes = event
  }

  variavel() {
    this.variavel2 = !this.variavel2;
  }
 }
